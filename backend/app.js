'use strict'

const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const path = require('path')
const morgan = require('morgan')
const cors = require('cors')
const exerciseRoute = require('./router/exerciseRoute')

// MIDDLEWARES
app.use(cors({
  "origin": "*",
  "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
  "preflightContinue": false,
  "optionsSuccessStatus": 204
}))
app.use(morgan('dev'))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

// DIRECTORIO PUBLICO ASSETS ESTATICOS - FRONTEND
app.use(express.static(path.join(__dirname, 'dist')))


app.use('/exercises', exerciseRoute)

module.exports = app