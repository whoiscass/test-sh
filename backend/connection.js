'use strict'

const mongoose = require('mongoose')
const app = require('./app')
const config = require('./config')

mongoose.Promise = global.Promise

mongoose.connect(config.db)

mongoose.connection.on('connected', (err) => {
  if(err) {
    console.log(`DATABASE connection problem ${err}`)
  }else {
    console.log(`Successfully connected to Test-SOCIALH4CK dev-DB ${config.db}`)
  }
})

app.listen(config.port, () => {
  console.log(`Test-SOCIALH4CK dev-server successfully running on port: ${config.port}`)
})