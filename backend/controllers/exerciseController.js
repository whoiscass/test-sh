'use strict'

const Exercise = require('../models/exercise')

module.exports = {
  createExercise: (req, res) => {
    let exercise = new Exercise(req.body)
    exercise.save()
    .then(r => {
      res.status(200).json(exercise)
    })
  },

  getExercises: (req, res) => {
    Exercise.find({})
    .then(r => {
      res.status(200).json(r)
    })
  },

  getExercise: (req, res) => {
    Exercise.find({_id: req.params.id})
    .then(r => {
      res.status(200).json(r)
    })
  },

  updateExercise: (req, res) => {
    let id = req.params.id
    let modified = req.body
    Exercise.findOneAndUpdate({_id: id}, modified)
    .then(r => {
      console.log(r)
      res.status(200).json(r)
    })
  },

  deleteExercise: (req, res) => {
    Exercise.findOneAndRemove({_id: req.params.id})
    .then(r => {
      res.status(200).json(r)
    })
  }
}
