webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- Fixed navbar -->\n<nav class=\"navbar navbar-default navbar-fixed-top\" style=\"margin-bottom: 0px;\">\n  <div class=\"container\">\n    <div class=\"navbar-header\">\n      <a [routerLink]=\"['/']\" class=\"navbar-brand\">Test-app socialh4ck</a>\n    </div>\n  </div>\n</nav>\n<div>\n    <router-outlet></router-outlet>\n</div>"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_router__ = __webpack_require__("../../../../../src/app/app.router.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_request_service__ = __webpack_require__("../../../../../src/app/services/request.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_wrapper_wrapper_component__ = __webpack_require__("../../../../../src/app/components/wrapper/wrapper.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_single_exercise_single_exercise_component__ = __webpack_require__("../../../../../src/app/components/single-exercise/single-exercise.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_8__components_wrapper_wrapper_component__["a" /* WrapperComponent */],
                __WEBPACK_IMPORTED_MODULE_9__components_single_exercise_single_exercise_component__["a" /* SingleExerciseComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_4_ng2_file_upload__["FileUploadModule"],
                __WEBPACK_IMPORTED_MODULE_5__app_router__["a" /* app_routing */]
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_6__services_request_service__["a" /* RequestService */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/app.router.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return app_routing; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_wrapper_wrapper_component__ = __webpack_require__("../../../../../src/app/components/wrapper/wrapper.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_single_exercise_single_exercise_component__ = __webpack_require__("../../../../../src/app/components/single-exercise/single-exercise.component.ts");



var app_routes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_1__components_wrapper_wrapper_component__["a" /* WrapperComponent */] },
    { path: 'exercises/:id', component: __WEBPACK_IMPORTED_MODULE_2__components_single_exercise_single_exercise_component__["a" /* SingleExerciseComponent */] },
    { path: '**', pathMatch: 'full', redirectTo: '' },
];
var app_routing = __WEBPACK_IMPORTED_MODULE_0__angular_router__["b" /* RouterModule */].forRoot(app_routes);


/***/ }),

/***/ "../../../../../src/app/components/single-exercise/single-exercise.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/single-exercise/single-exercise.component.html":
/***/ (function(module, exports) {

module.exports = "\t<div class=\"exercise\" *ngIf=\"!exercise\">\n\t\t<h3>Loading...</h3>\t\n\t</div>\n\t\n\t<div class=\"exercise\" *ngIf=\"exercise\" >\n\t\t<div class=\"container\">\n\t\t\t\t\t\t\t<div class=\"card\">\n\t\t\t\t\t\t\t\t<div class=\"card-top\">\n\t\t\t\t\t\t\t\t\t<span><b>Exercise</b></span>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"card-content\">\n\t\t\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t\t<form (ngSubmit)=\"onSubmit()\" >\n\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-3\">\n\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t\t\t\t\t<label for=\"name\">Name</label>\n\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"name\" name=\"exercise.name\" [(ngModel)] = \"exercise.name\">\n\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-3\">\n\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label for=\"typeExercise\">Exercise type</label>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"typeExercise\" name=\"typeExercise\" [(ngModel)] = \"exercise.typeExercise\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-3\">\n\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label for=\"Duration\">Duration <small>(minutes)</small></label>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" id=\"Duration\" name=\"duration\" [(ngModel)] = \"exercise.duration\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-3\">\n\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label for=\"createdBy\">Created by</label>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"createdBy\" name=\"createdBy\" [(ngModel)] = \"exercise.createdBy\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-3\">\n\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t\t\t<label for=\"name\">Level</label>\n\t\t\t\t\t\t\t\t\t\t<select class=\"form-control\" [(ngModel)]=\"exercise.levelDifficulty\" name=\"level\">\n\t\t\t\t\t\t\t\t\t\t\t<option value=\"{{ i }}\" *ngFor=\"let i of levels\">{{ i }}</option>\n\t\t\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class=\"col-md-3\">\n\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t\t\t<label for=\"name\">status</label>\n\t\t\t\t\t\t\t\t\t\t<select class=\"form-control\" [(ngModel)]=\"exercise.status\" name=\"status\">\n\t\t\t\t\t\t\t\t\t\t\t<option value=\"{{ s }}\" *ngFor=\"let s of status\">{{ s }}</option>\n\t\t\t\t\t\t\t\t\t\t</select>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div></div>\n\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<hr>\n\t\t\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-3\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t<label for=\"createdBy\">Image</label>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-default\">Add image...\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input ng2FileSelect [uploader] = 'uploaderi' type=\"file\" style=\"display: none;\" name=\"banner\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button style=\"display: inline-block\" class=\"btn btn-link btn-xs\" (click) = \"uploaderi.queue[uploaderi.queue.length - 1]?.remove()\" *ngIf=\"uploaderi.queue.length\" ><b>{{ uploaderi.queue[uploaderi.queue.length - 1].file.name }}</b></button>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div *ngIf=\"uploaderi.queue.length\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<br>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<small class=\"text-danger\">click attachment for delete it</small>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-3\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label for=\"createdBy\">Music</label>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-default\">Add music...\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input multiple ng2FileSelect [uploader] = 'uploader' type=\"file\" style=\"display: none;\" name=\"banner\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</label> <kbd>multiselect</kbd>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button style=\"display: inline-block\" *ngFor=\"let item of uploader.queue\" class=\"btn btn-link btn-xs\" (click) = \"item.remove()\"><b>{{ item.file.name }}</b></button>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div *ngIf=\"uploader.queue.length\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<br>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<small class=\"text-danger\">click attachment for delete it</small>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-3\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label for=\"createdBy\">Video</label>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-default\">Add video...\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input multiple ng2FileSelect [uploader] = 'uploaderv' type=\"file\" style=\"display: none;\" name=\"banner\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</label> <kbd>multiselect</kbd>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button style=\"display: inline-block\" *ngFor=\"let item of uploaderv.queue\" class=\"btn btn-link btn-xs\" (click) = \"item.remove()\"><b>{{ item.file.name }}</b></button>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div *ngIf=\"uploaderv.queue.length\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<br>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<small class=\"text-danger\"  >click attachment for delete it</small>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t<hr>\n\t\t\t\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-6\">\n\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"submit\" class=\"btn btn-primary\">Update</button>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t</form>\n\t\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t</div>"

/***/ }),

/***/ "../../../../../src/app/components/single-exercise/single-exercise.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SingleExerciseComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_request_service__ = __webpack_require__("../../../../../src/app/services/request.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ng2_file_upload__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SingleExerciseComponent = /** @class */ (function () {
    function SingleExerciseComponent(route, request) {
        var _this = this;
        this.route = route;
        this.request = request;
        this.levels = [0, 1, 2, 3, 4, 5];
        this.status = ['enable', 'disable'];
        this.uploader = new __WEBPACK_IMPORTED_MODULE_3_ng2_file_upload__["FileUploader"]({ url: 'http://localhost:5000/exercises/upload' });
        this.uploaderv = new __WEBPACK_IMPORTED_MODULE_3_ng2_file_upload__["FileUploader"]({ url: 'http://localhost:5000/exercises/upload' });
        this.uploaderi = new __WEBPACK_IMPORTED_MODULE_3_ng2_file_upload__["FileUploader"]({ url: 'http://localhost:5000/exercises/upload' });
        this.uploader.onAfterAddingFile = function (file) {
            file.withCredentials = false;
        };
        this.uploaderv.onAfterAddingFile = function (file) {
            file.withCredentials = false;
        };
        this.uploaderi.onAfterAddingFile = function (file) {
            file.withCredentials = false;
            if (_this.uploaderi.queue.length > 1) {
                _this.uploaderi.queue.shift();
            }
            console.log(_this.uploaderi.queue);
        };
        this.uploader.onCompleteItem = function (item, response, status, headers) {
            var asd = [].push(JSON.parse(response));
            _this.exercise.music = asd;
        };
        this.uploaderv.onCompleteItem = function (item, response, status, headers) {
            var qwe = [].push(JSON.parse(response));
            _this.exercise.videoTutor = qwe;
        };
        this.uploaderi.onCompleteItem = function (item, response, status, headers) {
            _this.exercise.mainImage = JSON.parse(response).sourcePath;
        };
    }
    SingleExerciseComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (p) {
            _this.id = p.id;
        });
        this.request.getExercise(this.id)
            .subscribe(function (r) {
            r.map(function (i) {
                _this.exercise = i;
                console.log(i);
            });
        });
    };
    SingleExerciseComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.uploader.queue.length)
            this.uploader.uploadAll();
        if (this.uploaderv.queue.length)
            this.uploaderv.uploadAll();
        if (this.uploaderi.queue.length)
            this.uploaderi.uploadAll();
        setTimeout(function () {
            _this.request.updateExercise(_this.exercise._id, _this.exercise)
                .subscribe(function (r) {
                console.log(r);
            });
        }, 2000);
    };
    SingleExerciseComponent.prototype.download = function (name) {
        this.request.getFile(name)
            .subscribe(function (r) {
            console.log(r);
        });
    };
    SingleExerciseComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-single-exercise',
            template: __webpack_require__("../../../../../src/app/components/single-exercise/single-exercise.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/single-exercise/single-exercise.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_2__services_request_service__["a" /* RequestService */]])
    ], SingleExerciseComponent);
    return SingleExerciseComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/wrapper/wrapper.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/wrapper/wrapper.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"exercise\" *ngIf=\"exercise\">\n  <div class=\"container\">\n          <div class=\"card\">\n            <div class=\"card-top\">\n              <span><b>Exercise</b></span>\n            </div>\n              <div class=\"card-content\">\n                  \n                      \n                <form (ngSubmit)=\"onSubmit()\" >\n                    <div class=\"row\">\n                    <div class=\"col-md-3\">\n                  <div class=\"form-group\">\n                    <label for=\"name\">Name</label>\n                    <input type=\"text\" class=\"form-control\" id=\"name\" name=\"exercise.name\" [(ngModel)] = \"exercise.name\">\n                  </div>\n                </div>\n                <div class=\"col-md-3\">\n                    <div class=\"form-group\">\n                        <label for=\"typeExercise\">Exercise type</label>\n                        <input type=\"text\" class=\"form-control\" id=\"typeExercise\" name=\"typeExercise\" [(ngModel)] = \"exercise.typeExercise\">\n                      </div>\n                </div>\n                <div class=\"col-md-3\">\n                  <div class=\"form-group\">\n                        <label for=\"Duration\">Duration <small>(minutes)</small></label>\n                        <input type=\"number\" class=\"form-control\" id=\"Duration\" name=\"duration\" [(ngModel)] = \"exercise.duration\">\n                      </div>\n                </div>\n                <div class=\"col-md-3\">\n                    <div class=\"form-group\">\n                        <label for=\"createdBy\">Created by</label>\n                        <input type=\"text\" class=\"form-control\" id=\"createdBy\" name=\"createdBy\" [(ngModel)] = \"exercise.createdBy\">\n                      </div>\n                </div>\n              </div>\n              <div class=\"row\">\n                <div class=\"col-md-3\">\n              <div class=\"form-group\">\n                <label for=\"name\">Level</label>\n                <select class=\"form-control\" [(ngModel)]=\"exercise.levelDifficulty\" name=\"level\">\n                  <option value=\"{{ i }}\" *ngFor=\"let i of levels\">{{ i }}</option>\n                </select>\n              </div>\n            </div>\n            <div class=\"col-md-3\">\n              <div class=\"form-group\">\n                <label for=\"name\">status</label>\n                <select class=\"form-control\" [(ngModel)]=\"exercise.status\" name=\"status\">\n                  <option value=\"{{ s }}\" *ngFor=\"let s of status\">{{ s }}</option>\n                </select>\n              </div>\n          </div></div>\n              <hr>\n              <div class=\"row\">\n                  <div class=\"col-md-3\">\n                      <label for=\"createdBy\">Image</label>\n                      <div class=\"form-group\">\n                          <label class=\"btn btn-default\">Add image...\n                            <input ng2FileSelect [uploader] = 'uploaderi' type=\"file\" style=\"display: none;\" name=\"banner\">\n                          </label>\n                        </div>\n                        <button style=\"display: inline-block\" class=\"btn btn-link btn-xs\" (click) = \"uploaderi.queue[uploaderi.queue.length - 1]?.remove()\" *ngIf=\"uploaderi.queue.length\" ><b>{{ uploaderi.queue[uploaderi.queue.length - 1].file.name }}</b></button>\n                        <div *ngIf=\"uploaderi.queue.length\">\n                          <br>\n                          <small class=\"text-danger\">click attachment for delete it</small>\n                        </div>\n                </div>\n                  <div class=\"col-md-3\">\n                          <label for=\"createdBy\">Music</label>\n                          <div class=\"form-group\">\n                              <label class=\"btn btn-default\">Add music...\n                                <input multiple ng2FileSelect [uploader] = 'uploader' type=\"file\" style=\"display: none;\" name=\"banner\">\n                              </label> <kbd>multiselect</kbd>\n                            </div>\n                            <button style=\"display: inline-block\" *ngFor=\"let item of uploader.queue\" class=\"btn btn-link btn-xs\" (click) = \"item.remove()\"><b>{{ item.file.name }}</b></button>\n                            <div *ngIf=\"uploader.queue.length\">\n                              <br>\n                              <small class=\"text-danger\">click attachment for delete it</small>\n                            </div>\n                    </div>\n                    <div class=\"col-md-3\">\n                        <label for=\"createdBy\">Video</label>\n                        <div class=\"form-group\">\n                            <label class=\"btn btn-default\">Add video...\n                              <input multiple ng2FileSelect [uploader] = 'uploaderv' type=\"file\" style=\"display: none;\" name=\"banner\">\n                            </label> <kbd>multiselect</kbd>\n                          </div>\n                          <button style=\"display: inline-block\" *ngFor=\"let item of uploaderv.queue\" class=\"btn btn-link btn-xs\" (click) = \"item.remove()\"><b>{{ item.file.name }}</b></button>\n                          <div *ngIf=\"uploaderv.queue.length\">\n                              <br>\n                              <small class=\"text-danger\"  >click attachment for delete it</small>\n                            </div>\n                      </div>\n              </div>\n              \n              <hr>\n              <div class=\"row\">\n                <div class=\"col-md-6\">\n                    <button type=\"submit\" class=\"btn btn-primary\">Submit</button>\n                </div>\n              </div>\n                </form>\n                \n              </div>\n            </div>\n  </div>\n</div>\n<div class=\"wrapper\">\n  <div class=\"container\">\n      <div class=\"row\">\n      <!-- Card Projects -->\n        <div class=\"col-md-4\" *ngFor=\"let item of exercises\">\n          <div class=\"card\">\n            <div class=\"card-image\">\n              <img class=\"img-responsive\" width=\"100%\" src=\"{{ item.mainImage }}\">\n            </div>\n            <a routerLink=\"/exercises/{{item._id}}\">\n            <div class=\"card-content\">\n              <span class=\"card-title\">{{ item.name }}</span>\n              <p><b>created by</b> {{ item.createdBy}}</p>\n              <span>{{ item.creationDate }}</span>\n            </div>\n          </a>\n            \n            <div class=\"card-action\">\n              <button class=\"btn btn-link\" (click) = \"remove(item._id)\">DELETE</button>\n            </div>\n          </div>\n            \n        </div>\n    </div>\n  </div>\n      \n</div>\n\n"

/***/ }),

/***/ "../../../../../src/app/components/wrapper/wrapper.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WrapperComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_file_upload__ = __webpack_require__("../../../../ng2-file-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_request_service__ = __webpack_require__("../../../../../src/app/services/request.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var WrapperComponent = /** @class */ (function () {
    function WrapperComponent(request) {
        var _this = this;
        this.request = request;
        this.exercises = [];
        this.levels = [0, 1, 2, 3, 4, 5];
        this.status = ['enable', 'disable'];
        this.exercise = {
            name: '',
            typeExercise: '',
            mainImage: '',
            duration: 0,
            lights: [],
            music: [],
            video: [],
            createdBy: ''
        };
        this.uploader = new __WEBPACK_IMPORTED_MODULE_1_ng2_file_upload__["FileUploader"]({ url: 'http://localhost:5000/exercises/upload' });
        this.uploaderv = new __WEBPACK_IMPORTED_MODULE_1_ng2_file_upload__["FileUploader"]({ url: 'http://localhost:5000/exercises/upload' });
        this.uploaderi = new __WEBPACK_IMPORTED_MODULE_1_ng2_file_upload__["FileUploader"]({ url: 'http://localhost:5000/exercises/upload' });
        this.uploader.onAfterAddingFile = function (file) {
            file.withCredentials = false;
        };
        this.uploaderv.onAfterAddingFile = function (file) {
            file.withCredentials = false;
        };
        this.uploaderi.onAfterAddingFile = function (file) {
            file.withCredentials = false;
            if (_this.uploaderi.queue.length > 1) {
                _this.uploaderi.queue.shift();
            }
            console.log(_this.uploaderi.queue);
        };
        this.uploader.onCompleteItem = function (item, response, status, headers) {
            _this.exercise.music.push(JSON.parse(response));
            _this.exercise.music.filter(function (m) {
                m.volumen = _this.exercise.music.indexOf(m) + 1;
            });
        };
        this.uploaderv.onCompleteItem = function (item, response, status, headers) {
            _this.exercise.video.push(JSON.parse(response));
        };
        this.uploaderi.onCompleteItem = function (item, response, status, headers) {
            _this.exercise.mainImage = JSON.parse(response).sourcePath;
        };
    }
    WrapperComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.request.getExercises()
            .subscribe(function (r) {
            r.map(function (i) {
                _this.exercises.push(i);
            });
        });
        console.log(this.exercises);
    };
    WrapperComponent.prototype.onSubmit = function () {
        var _this = this;
        if (this.uploader.queue.length)
            this.uploader.uploadAll();
        if (this.uploaderv.queue.length)
            this.uploaderv.uploadAll();
        if (this.uploaderi.queue.length)
            this.uploaderi.uploadAll();
        setTimeout(function () {
            _this.request.createExercise(_this.exercise)
                .subscribe(function (r) {
                _this.exercises.push(r);
            });
        }, 2000);
    };
    WrapperComponent.prototype.remove = function (id) {
        var _this = this;
        this.request.deleteExercise(id)
            .subscribe(function (r) {
            _this.exercises.splice(_this.exercises.indexOf(r), 1);
        });
    };
    WrapperComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-wrapper',
            template: __webpack_require__("../../../../../src/app/components/wrapper/wrapper.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/wrapper/wrapper.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_request_service__["a" /* RequestService */]])
    ], WrapperComponent);
    return WrapperComponent;
}());



/***/ }),

/***/ "../../../../../src/app/services/request.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RequestService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RequestService = /** @class */ (function () {
    function RequestService(http) {
        this.http = http;
        this.exercises = [];
    }
    RequestService.prototype.createExercise = function (exercise) {
        return this.http.post('http://127.0.0.1:5000/exercises', exercise)
            .map(function (response) {
            return response;
        });
    };
    RequestService.prototype.getExercises = function () {
        return this.http.get('http://127.0.0.1:5000/exercises')
            .map(function (response) {
            return response;
        });
    };
    RequestService.prototype.getExercise = function (id) {
        return this.http.get('http://127.0.0.1:5000/exercises/' + id)
            .map(function (response) {
            return response;
        });
    };
    RequestService.prototype.deleteExercise = function (id) {
        return this.http.delete('http://127.0.0.1:5000/exercises/' + id)
            .map(function (response) {
            return response;
        });
    };
    RequestService.prototype.updateExercise = function (id, modified) {
        return this.http.put('http://127.0.0.1:5000/exercises/' + id, modified)
            .map(function (response) {
            return response;
        });
    };
    RequestService.prototype.getFile = function (name) {
        return this.http.post('http://127.0.0.1:5000/exercises/download', name, {
            responseType: 'blob',
            headers: new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["c" /* HttpHeaders */]({
                'Content-Type': 'application/json'
            })
        })
            .map(function (response) {
            return response;
        });
    };
    RequestService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], RequestService);
    return RequestService;
}());



/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map