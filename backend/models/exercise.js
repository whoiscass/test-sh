'use strict'

const mongoose = require('mongoose')

const ExerciseSchema = new mongoose.Schema({

	name: {type: String, default: 'None'},
	typeExercise: {type: String, default: 'None'},
	mainImage: {type: String, default: null},
  levelDifficulty: {type: Number, default: 0, enum: [0, 1, 2, 3, 4, 5]},
  duration: Number,
  music: [
    {
      name: {type: String, default: 'None'},
      sourcePath: {type: String, default: 'None'},
      volumen: {type: Number, default: 0},
    }
  ],
  videoTutor: [
    {
      name: {type: String, default: 'None'},
      sourcePath: {type: String, default: 'None'}
    }
  ],
  createdBy: {type: String, default: 'None'},
  creationDate: {type: String, default: 'None'},
  status: {type: String, default: 'enable', enum: ['enable', 'disable']}
})

const Exercise = mongoose.model('Exercise', ExerciseSchema)

module.exports = Exercise