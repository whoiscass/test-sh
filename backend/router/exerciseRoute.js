'use strict'

const express = require('express')
const path = require('path')
const router = express.Router()
const multer = require('multer')
const exerciseController = require('../controllers/exerciseController')

const store = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './uploads')
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + '.' + file.originalname)
  }
})

let upload = multer({ storage: store }).single('file')

router.route('/')
  .get(exerciseController.getExercises)
  .post(exerciseController.createExercise)

router.route('/:id')
  .get(exerciseController.getExercise)
  .put(exerciseController.updateExercise)
  .delete(exerciseController.deleteExercise)

router.route('/upload')
  .post((req, res, next) => {
      upload(req, res, function(err) {
        let filepath = path.join(__dirname, '../uploads') + '/' + req.file.filename
        res.json({name: req.file.filename, sourcePath: filepath})
      })
  })

  router.route('/download')
    .post((req, res, next) => {
      let filepath = path.join(__dirname, '../uploads') + '/' + req.body
      res.sendFile(filepath)
    })

module.exports = router