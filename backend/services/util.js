'use strict'

module.exports = { 
  char: (c) => {
    const charRegex = /^([A-Za-z])*$/
    if(!charRegex.test(c)) false
    return true
  },

  spaceChar: (c) => {
    const spaceCharRegex = /^([A-Za-z\s])*$/
    if(!spaceCharRegex.test(c)) false
    return true
  },

  num: (c) => {
    const numRegex = /^([0-9])*$/
    if(!numRegex.test(c)) false
    return true
  }
}