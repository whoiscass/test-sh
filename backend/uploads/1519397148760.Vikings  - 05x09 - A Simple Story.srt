1
00:00:06,400 --> 00:00:07,880
<i>Anteriormente en Vikings...</i>

2
00:00:07,933 --> 00:00:09,840
Necesitamos construir un templo.

3
00:00:09,900 --> 00:00:11,200
�Por qu� deber�amos construir
un templo...

4
00:00:11,267 --> 00:00:12,399
antes que nuestras casas?

5
00:00:12,400 --> 00:00:13,320
Vinimos aqu� a morir.

6
00:00:13,368 --> 00:00:16,360
�No! Convertiremos esta tierra
en un lugar maravilloso.

7
00:00:16,433 --> 00:00:17,920
Deber�as hablar con tu padre.

8
00:00:18,000 --> 00:00:19,520
Est� muy ocupado entrenando
a mi hermano...

9
00:00:19,567 --> 00:00:21,199
para ser un gran guerrero.

10
00:00:21,200 --> 00:00:24,440
Si quieres guerra,
tengamos guerra.

11
00:00:24,500 --> 00:00:26,320
Me retirar� con un tercio
de nuestro ej�rcito...

12
00:00:26,367 --> 00:00:28,399
y enviar� a Hvitserk
a que los ataque de sorpresa.

13
00:00:28,400 --> 00:00:29,560
De acuerdo.

14
00:00:29,633 --> 00:00:30,967
�Ataquen!

15
00:00:38,367 --> 00:00:40,040
- �Nos necesitan!
- �Es demasiado tarde!

16
00:00:40,100 --> 00:00:41,480
�Retirada!

17
00:00:44,267 --> 00:00:45,280
�Qui�n es este?

18
00:00:45,333 --> 00:00:47,760
- Es un sacerdote saj�n.
- No lo mates.

19
00:00:47,833 --> 00:00:49,967
- �Por qu�?
- No s� por qu�.

20
00:03:14,333 --> 00:03:16,120
Sus heridas sanar�n.

21
00:03:25,600 --> 00:03:27,240
Salvaste mi vida.

22
00:03:27,300 --> 00:03:28,867
Gracias.

23
00:03:31,733 --> 00:03:35,100
Pero si no lo hubiera hecho,
estar�as en el cielo.

24
00:03:36,000 --> 00:03:38,360
Tengo entendido
que eres un sacerdote cristiano.

25
00:03:38,400 --> 00:03:40,640
�No es ah�
donde preferir�as estar?

26
00:03:40,700 --> 00:03:43,200
Soy sacerdote,

27
00:03:43,267 --> 00:03:45,320
pero tambi�n soy un hombre.

28
00:03:46,633 --> 00:03:48,100
Amo a Dios.

29
00:03:49,533 --> 00:03:50,760
Tambi�n amo la vida.

30
00:03:50,800 --> 00:03:55,600
�Qu� amas tanto de la vida?

31
00:03:57,533 --> 00:03:59,300
La alegr�a.

32
00:04:00,533 --> 00:04:01,880
El dolor.

33
00:04:07,367 --> 00:04:10,200
�Y amas a los seres humanos?

34
00:04:10,267 --> 00:04:12,800
Nuestro Se�or am�
a todos los seres humanos.

35
00:04:12,867 --> 00:04:14,433
Su amor fue...

36
00:04:15,533 --> 00:04:18,233
�gape, no eros.

37
00:04:19,500 --> 00:04:21,520
Un amor grande y acogedor.

38
00:04:22,633 --> 00:04:27,120
�Tambi�n tienes el mismo amor,
grande y acogedor?

39
00:04:27,200 --> 00:04:31,067
�Amas a los hombres y mujeres
de la misma forma?

40
00:04:33,933 --> 00:04:36,167
No soy nuestro Se�or.

41
00:04:37,533 --> 00:04:40,999
�gape es
demasiado dif�cil para m�.

42
00:04:41,000 --> 00:04:42,720
Amo a las mujeres,

43
00:04:43,967 --> 00:04:45,933
no solo espiritualmente.

44
00:04:48,467 --> 00:04:51,320
No tenemos problemas
en amar el esp�ritu...

45
00:04:51,367 --> 00:04:53,999
y la carne.

46
00:04:54,000 --> 00:04:57,320
Nuestros Dioses lo incentivan.

47
00:04:57,400 --> 00:04:59,000
�No tienes culpas?

48
00:04:59,067 --> 00:05:01,300
�Nada de remordimientos?

49
00:05:03,700 --> 00:05:06,000
- No.
- Te envidio.

50
00:05:10,033 --> 00:05:12,500
Pas� muchos a�os de mi vida...

51
00:05:13,567 --> 00:05:15,467
viviendo en el pecado.

52
00:05:23,467 --> 00:05:25,360
�Por qu� no me mataste?

53
00:05:29,400 --> 00:05:30,720
No lo s�.

54
00:05:32,767 --> 00:05:34,600
Quiz� lo averig�emos.

55
00:05:41,067 --> 00:05:44,067
Fuiste demasiado listo, Ivar.

56
00:05:45,567 --> 00:05:49,167
Ten�amos la ventaja
y la perdimos.

57
00:05:51,500 --> 00:05:55,033
Seg�n recuerdo,
t� aprobaste el plan.

58
00:05:56,067 --> 00:05:59,120
Hasta entonces, habias probado

59
00:05:59,167 --> 00:06:03,233
ser un buen estratega.
Cre� en t�.

60
00:06:05,067 --> 00:06:09,100
Puede que no apruebe tus planes
tan r�pido la pr�xima vez.

61
00:06:11,667 --> 00:06:15,320
Haz lo que consideres mejor,
Rey Harald.

62
00:06:15,400 --> 00:06:18,280
Ivar, el t�o Rollo
me dijo algo una vez...

63
00:06:18,333 --> 00:06:21,833
en su barco, cuando regresamos
del Mediterr�neo,

64
00:06:23,000 --> 00:06:25,680
que si alguna vez necesitaba
su ayuda,

65
00:06:25,733 --> 00:06:30,133
solo ten�a que pedirla.

66
00:06:33,567 --> 00:06:35,040
�Qu� opinas?

67
00:06:38,233 --> 00:06:39,840
Vamos.

68
00:06:39,900 --> 00:06:43,120
�Qu� opinas?

69
00:06:43,167 --> 00:06:45,640
�Quieres que los franceses
nos ayuden?

70
00:06:47,933 --> 00:06:51,767
Opino que deber�an partir
ma�ana al amanecer.

71
00:07:04,067 --> 00:07:07,400
<i>- �S�, as�!
- Continuen.</i>

72
00:07:07,467 --> 00:07:12,000
<i>- Sigan.
- Vamos, una m�s.</i>

73
00:07:20,767 --> 00:07:23,400
He estado pensando en Eyvind,

74
00:07:23,467 --> 00:07:25,400
sobre su comportamiento.

75
00:07:25,633 --> 00:07:27,360
�Qu� hay sobre eso?

76
00:07:27,833 --> 00:07:31,240
Le dijiste que si no le gustaba
este lugar,

77
00:07:31,300 --> 00:07:33,160
siempre podr�a regresar a casa.

78
00:07:33,233 --> 00:07:34,920
Y el d�jo que destruimos
todas esas posibilidades

79
00:07:34,967 --> 00:07:36,920
y que nunca podremos
regresar a casa.

80
00:07:36,967 --> 00:07:40,360
Pero no es cierto,
siempre puede navegar a casa.

81
00:07:40,433 --> 00:07:43,240
�Qu� lo detiene?

82
00:07:43,300 --> 00:07:45,560
- Nada.
- No.

83
00:07:45,633 --> 00:07:47,680
Tiene una raz�n para quedarse.

84
00:07:47,733 --> 00:07:49,640
Lejos de querer
irse de esta isla,

85
00:07:49,700 --> 00:07:52,480
quiere quedarse
porque quiere ser el rey.

86
00:07:52,533 --> 00:07:55,520
Nunca podr�a ser
rey en Kattegat.

87
00:07:55,600 --> 00:07:58,200
Pero aqu�,
en una tierra deshabitada,

88
00:07:58,267 --> 00:08:03,200
un mundo nuevo, hostil o no,
�l podr�a ser el rey de todo.

89
00:08:03,267 --> 00:08:06,120
Pero primero debe
socavar tu autoridad,

90
00:08:06,200 --> 00:08:08,480
debe poner a la gente
en tu contra.

91
00:08:08,533 --> 00:08:11,120
Es lo que �l
ha estado tratando de hacer.

92
00:08:12,400 --> 00:08:14,320
�No lo puedes ver?

93
00:08:14,400 --> 00:08:17,520
He observado a Eyvind
desde el inicio.

94
00:08:17,567 --> 00:08:21,800
En verdad, �l est� tratando
de provocar una reacci�n.

95
00:08:21,867 --> 00:08:24,999
Quiere que todos sucumbamos
al caos y la violencia,

96
00:08:25,000 --> 00:08:27,200
entonces podr� obtener el poder.

97
00:08:27,267 --> 00:08:31,160
Pero no podemos darle
esa satisfacci�n.

98
00:08:31,233 --> 00:08:33,320
Debemos permanecer calmados.

99
00:08:37,467 --> 00:08:39,360
�En qu� est�s pensando?

100
00:08:44,800 --> 00:08:48,600
- �Pienas en nuestro beb�?
- Por supuesto.

101
00:08:51,667 --> 00:08:53,040
Me alegra.

102
00:08:57,500 --> 00:09:00,680
Si vamos de nuevo a la guerra,

103
00:09:00,733 --> 00:09:03,000
no me digas que no puedo pelear.

104
00:09:04,300 --> 00:09:06,360
D�jame hacer

105
00:09:06,433 --> 00:09:08,720
lo que tenga que hacer,

106
00:09:08,767 --> 00:09:10,767
lo que quiero hacer.

107
00:09:14,800 --> 00:09:18,333
Quiero que nuestro beb� escuche
los gritos de la batalla.

108
00:09:21,767 --> 00:09:23,840
Quiero que sea un guerrero.

109
00:09:24,967 --> 00:09:27,640
Deber�as estar feliz,

110
00:09:27,700 --> 00:09:30,440
pero no est�s feliz.

111
00:09:30,500 --> 00:09:32,400
Perdimos una batalla.

112
00:09:34,633 --> 00:09:36,240
Muchos murieron.

113
00:09:38,833 --> 00:09:41,520
�De verdad crees
que deber�a estar feliz?

114
00:09:41,600 --> 00:09:42,880
�Halen!

115
00:09:45,533 --> 00:09:46,480
�Bien!

116
00:09:46,534 --> 00:09:48,100
�Halen!

117
00:09:50,400 --> 00:09:51,920
�Sost�nganlo!

118
00:09:55,933 --> 00:09:56,880
�Oye, Bul!

119
00:09:56,934 --> 00:09:58,720
Ven a ayudarnos.

120
00:09:58,767 --> 00:10:00,360
Thor es pesado.

121
00:10:01,200 --> 00:10:04,200
No queremos involucrarnos
con su templo, Thorgrim.

122
00:10:04,267 --> 00:10:06,520
Es su problema.

123
00:10:06,600 --> 00:10:09,480
Tampoco queremos pagar
impuestos por eso.

124
00:10:09,533 --> 00:10:11,120
Por la manera en que hablas,

125
00:10:11,167 --> 00:10:13,367
no suenas como un toro.

126
00:10:14,400 --> 00:10:18,040
Suenas como un perro,
entonces te llamar� as�,

127
00:10:18,100 --> 00:10:19,600
El perro Bul.

128
00:10:26,333 --> 00:10:27,880
Dilo de nuevo.

129
00:10:30,133 --> 00:10:32,567
Hazte un favor, Bul, al�jate.

130
00:10:38,667 --> 00:10:40,200
Dilo de nuevo.

131
00:10:46,833 --> 00:10:48,320
El perro Bul.

132
00:10:51,567 --> 00:10:55,633
<i>- Paren.
- No peleen.</i>

133
00:10:59,233 --> 00:11:00,560
�Kjetill!

134
00:11:00,600 --> 00:11:03,433
- Su�ltalo.
- �Padre, detente!

135
00:11:04,767 --> 00:11:06,120
�Detente!

136
00:11:07,367 --> 00:11:09,000
Bajen sus armas.

137
00:11:18,100 --> 00:11:19,440
Ven, Bul.

138
00:11:21,367 --> 00:11:23,520
El chico aprendi� su lecci�n.

139
00:11:27,033 --> 00:11:28,840
<i>�V�monos, Bul!</i>

140
00:11:52,183 --> 00:11:54,090
�Est�s seguro
de que eran soldados francos?

141
00:11:54,150 --> 00:11:56,370
S�, estoy segura.

142
00:11:56,417 --> 00:11:58,317
<i>Son muchos.</i>

143
00:11:59,550 --> 00:12:02,970
<i>Vi muchos barcos.</i>

144
00:12:03,050 --> 00:12:05,240
<i>Demasiados para contar.</i>

145
00:12:11,083 --> 00:12:14,250
�Por qu� Rollo enviar�a soldados
para pelear por Hvitserk y Ivar?

146
00:12:14,317 --> 00:12:15,650
No lo s�.

147
00:12:16,550 --> 00:12:20,650
Quiz� podr�a ir a hablar con �l.

148
00:12:22,483 --> 00:12:24,890
- Ir� contigo.
- No.

149
00:12:24,950 --> 00:12:26,570
Ivar te matar�a.

150
00:12:27,550 --> 00:12:29,170
<i>Ir� solo.</i>

151
00:12:29,917 --> 00:12:33,970
<i>Quiz� podamos llegar
a alguna clase de acuerdo.</i>

152
00:12:34,017 --> 00:12:36,690
Quiz� el Rey Harald y Rollo
entren en raz�n.

153
00:12:36,750 --> 00:12:38,530
No contar�a con eso.

154
00:12:43,317 --> 00:12:46,150
<i>Ahora est�n
en una posici�n de fuerza.</i>

155
00:12:53,950 --> 00:12:55,650
�D�nde est� Rollo?

156
00:12:56,983 --> 00:12:59,050
<i>Rollo no pudo venir.</i>

157
00:12:59,450 --> 00:13:01,680
Ten�a muchas responsabilidades.

158
00:13:02,750 --> 00:13:06,730
Pero me dijo que regresar�a
para celebrar con nosotros,

159
00:13:06,783 --> 00:13:08,690
despu�s de la victoria.

160
00:13:14,717 --> 00:13:16,490
Solo pidi� una cosa.

161
00:13:18,517 --> 00:13:20,350
�Qu� cosa?

162
00:13:23,017 --> 00:13:25,330
Que le perdonemos
la vida a Bjorn.

163
00:13:41,750 --> 00:13:43,410
Quiz� lo hagamos.

164
00:13:49,750 --> 00:13:53,890
<i>Este templo est� dedicado
al viejo Thor,</i>

165
00:13:53,950 --> 00:13:55,730
<i>Thor el Lanzador,</i>

166
00:13:55,783 --> 00:13:58,170
<i>Thor el Vagabundo Blanco,</i>

167
00:13:58,217 --> 00:14:00,330
<i>el hijo de Od�n,</i>

168
00:14:00,383 --> 00:14:02,090
el dios de los granjeros,

169
00:14:02,150 --> 00:14:06,250
<i>el dios que pesc� y pele�
contra la serpiente del mundo.</i>

170
00:14:29,417 --> 00:14:31,930
El dios que pele�
contra un gigante...

171
00:14:31,983 --> 00:14:36,370
y al que dejaron por siempre
con una piedra en su cabeza.

172
00:14:36,450 --> 00:14:40,810
Dios del trueno,
dios de la gente ordinaria,

173
00:14:40,883 --> 00:14:42,730
escucha nuestra plegaria.

174
00:14:42,783 --> 00:14:44,570
<i>�Escucha nuestra plegaria!</i>

175
00:14:44,617 --> 00:14:49,217
Bendice nuestro templo y recibe
nuestro sacrificio en tu nombre.

176
00:15:39,650 --> 00:15:41,210
Regr�salo.

177
00:15:41,283 --> 00:15:42,850
Es algo sagrado.

178
00:15:42,917 --> 00:15:44,850
Thor est� aqu�.

179
00:15:44,917 --> 00:15:47,650
D�melo. Dame la sangre.

180
00:15:51,383 --> 00:15:53,650
Muy bien, toma.

181
00:16:00,117 --> 00:16:01,983
<i>�Floki, no!</i>

182
00:16:41,550 --> 00:16:44,110
<i>�Quiero bailar con el cristiano!</i>

183
00:16:45,883 --> 00:16:48,417
<i>�Arr�strate, cristiano!</i>

184
00:17:03,517 --> 00:17:05,670
<i>�Levantate, cristiano!</i>

185
00:17:13,683 --> 00:17:15,480
�Qu� est�s haciendo?

186
00:17:16,183 --> 00:17:18,040
Quer�a hablar contigo.

187
00:17:21,783 --> 00:17:23,880
�De qu� quer�as hablarme?

188
00:17:23,950 --> 00:17:29,510
De todo, los dioses,
la vida y la muerte.

189
00:17:29,583 --> 00:17:33,270
De mi alma, el ahorcado.

190
00:17:33,317 --> 00:17:35,750
Desespero, esperanza.

191
00:17:36,950 --> 00:17:38,583
Vida eterna.

192
00:17:41,483 --> 00:17:43,070
�Por qu� conmigo?

193
00:17:43,117 --> 00:17:45,710
Creo que quiz� tengas
algunas respuestas.

194
00:17:46,683 --> 00:17:48,717
�Qu� te hizo pensar eso?

195
00:17:50,850 --> 00:17:52,480
�Crees en la fe?

196
00:17:56,817 --> 00:17:58,040
Claro.

197
00:17:59,150 --> 00:18:01,280
La fe nos uni�.

198
00:18:01,350 --> 00:18:03,883
No te halagues.

199
00:18:06,850 --> 00:18:08,050
No.

200
00:18:09,250 --> 00:18:11,160
Creo que somos iguales.

201
00:18:12,183 --> 00:18:14,200
Creo que tambi�n lo sabes.

202
00:18:15,217 --> 00:18:17,310
Estoy interesado en ti.

203
00:18:17,383 --> 00:18:18,850
Fascinado.

204
00:18:21,117 --> 00:18:22,790
Quiero saber m�s.

205
00:18:23,617 --> 00:18:27,550
Creo que tenemos
m�s en com�n de lo que crees.

206
00:18:30,283 --> 00:18:33,217
Tambi�n hay una urgencia
en esta conversaci�n.

207
00:18:34,750 --> 00:18:37,550
�Cu�l es la urgencia, sacerdote?

208
00:18:37,617 --> 00:18:40,483
S� que pelearemos de nuevo
y pronto.

209
00:18:42,117 --> 00:18:44,430
No tienes que pelear
por nosotros.

210
00:18:44,717 --> 00:18:48,590
Por supuesto que s�.
Por eso salvaste mi vida.

211
00:18:48,650 --> 00:18:53,070
Esperabas que tomara mi espada

212
00:18:53,117 --> 00:18:54,910
y peleara por ti,

213
00:18:54,983 --> 00:18:57,150
en contra de Ivar.

214
00:18:58,650 --> 00:19:00,040
�Lo har�s?

215
00:19:04,717 --> 00:19:07,583
Pelear� por ti, Lagertha.

216
00:19:09,450 --> 00:19:12,150
Morir� por ti.

217
00:19:22,583 --> 00:19:24,440
Aunque no me conozcas.

218
00:19:26,250 --> 00:19:27,750
Te conozco.

219
00:19:29,717 --> 00:19:32,917
Te he conocido toda mi vida.

220
00:19:40,150 --> 00:19:42,470
�En verdad quieres
volver a pecar?

221
00:19:53,183 --> 00:19:55,160
No deb� casarme con ella.

222
00:19:55,217 --> 00:19:56,760
Era muy joven.

223
00:19:58,350 --> 00:20:01,430
Ella no es qui�n yo pensaba.

224
00:20:01,483 --> 00:20:03,910
Tiene tales ambiciones.

225
00:20:03,983 --> 00:20:07,790
Dej� a mis hijos
al cuidado de Margrette.

226
00:20:07,850 --> 00:20:09,520
No los lastimar�.

227
00:20:15,450 --> 00:20:18,360
Parece que volveremos
a entrar en batalla pronto.

228
00:20:19,750 --> 00:20:21,680
Cualquiera de los dos
podr�a morir.

229
00:20:21,750 --> 00:20:24,320
Es casi seguro
que uno de los dos morir�.

230
00:20:26,450 --> 00:20:27,680
No quiero morir sin decir...

231
00:20:27,750 --> 00:20:30,680
que desar�a que fueras t�
quien llevara a mi hijo.

232
00:21:13,517 --> 00:21:14,990
�Margrette!

233
00:21:15,050 --> 00:21:18,050
Margrette, �qu� sucede?
Te est�bamos llamando.

234
00:21:24,883 --> 00:21:26,183
Mam�.

235
00:21:29,883 --> 00:21:31,850
Qu� est�pidos son.

236
00:21:33,083 --> 00:21:35,910
�Qu� creen que su madre
est� haciendo ahora?

237
00:21:35,983 --> 00:21:38,630
Quiz� se est� preparando
para pelear.

238
00:21:38,683 --> 00:21:40,670
Quiz� est� muerta.

239
00:21:40,717 --> 00:21:42,950
Quiz� ya muri� en la batalla.

240
00:21:43,017 --> 00:21:44,600
No est� muerta.

241
00:21:45,517 --> 00:21:48,910
Mi consejo es
que te olvides de ella.

242
00:21:48,983 --> 00:21:51,030
No pienses en ella.

243
00:21:51,083 --> 00:21:54,250
�No imagines que va a regresar!

244
00:21:56,617 --> 00:21:58,917
Excepto como un fantasma.

245
00:22:07,000 --> 00:22:08,720
�D�nde est� Rollo?

246
00:22:09,300 --> 00:22:11,540
�D�nde est� mi t�o?

247
00:22:11,600 --> 00:22:13,320
Fui a reunirme con �l.

248
00:22:13,367 --> 00:22:16,380
Te env�a su amor,
Bjorn Brazo de Hierro

249
00:22:16,433 --> 00:22:19,220
y espera que no luches
contra sus fuerzas.

250
00:22:19,267 --> 00:22:22,250
�Entonces por qu� le brindar�a
sus fuerzas a ustedes?

251
00:22:22,300 --> 00:22:24,360
No dijo por qu�, es la verdad.

252
00:22:24,400 --> 00:22:27,260
Solo que estaba
preparado para hacerlo.

253
00:22:27,333 --> 00:22:32,940
Puedo decirte que Rollo ve
con justicia nuestra causa.

254
00:22:33,000 --> 00:22:35,940
Lagertha asesin� a nuestra madre

255
00:22:36,000 --> 00:22:37,720
y usurp� su reino.

256
00:22:40,800 --> 00:22:43,060
Todo eso est�
en el pasado, Ivar.

257
00:22:43,867 --> 00:22:47,233
Debo vengar
el asesinato de mi madre.

258
00:22:48,800 --> 00:22:51,467
Creo que har�as lo mismo
si fueras yo.

259
00:22:58,700 --> 00:23:00,900
Por el amor a nuestro padre...

260
00:23:03,200 --> 00:23:06,860
y por el bien del legado
que dej� y todo en lo que cre�a,

261
00:23:06,933 --> 00:23:08,610
te lo pido, Ivar,

262
00:23:10,000 --> 00:23:12,840
no pongas las vidas
de nuestra gente en riesgo.

263
00:23:17,533 --> 00:23:19,780
La �nica raz�n
por la que dices eso

264
00:23:19,833 --> 00:23:24,433
es porque vez todo el poder
reunido en tu contra.

265
00:23:25,967 --> 00:23:29,240
Si a�n pensaras que podr�as
ganar no estar�as aqu�, Bjorn.

266
00:23:31,300 --> 00:23:33,090
De hecho, tienes miedo.

267
00:23:33,133 --> 00:23:34,730
No tengo miedo.

268
00:23:38,500 --> 00:23:40,290
Esto no cambia nada.

269
00:23:42,100 --> 00:23:43,300
Bien.

270
00:23:45,500 --> 00:23:47,090
��Qu� es esto?!

271
00:23:50,333 --> 00:23:55,620
Sabes muy bien
que esta no es nuestra manera.

272
00:23:55,667 --> 00:23:57,540
�No es nuestra manera!

273
00:24:07,267 --> 00:24:09,260
Val�a la pena intentarlo.

274
00:24:17,900 --> 00:24:20,767
Lamento que Rollo tuviera
que involucrarse.

275
00:24:27,867 --> 00:24:30,540
�No tiene suficiente
mundo para conquistar?

276
00:24:45,500 --> 00:24:46,740
Mis Se�ores,

277
00:24:46,800 --> 00:24:49,940
he convocado este concejo
porque es imperativo

278
00:24:50,000 --> 00:24:54,740
considerar los nuevos peligros
que enfrentamos.

279
00:24:54,800 --> 00:24:59,140
Despu�s de los estragos causados
por el gran ej�rcito pagano,

280
00:24:59,200 --> 00:25:02,660
pensamos que la tormenta pas�.

281
00:25:02,733 --> 00:25:04,420
Ese gran ej�rcito pagano

282
00:25:04,467 --> 00:25:07,660
demostr� ser el precursor
de m�s ataques.

283
00:25:07,733 --> 00:25:11,900
Ahora sus ej�rcitos y flotas
vienen de todos lados.

284
00:25:11,967 --> 00:25:14,500
Atacan Escocia e Irlanda
con impunidad

285
00:25:14,567 --> 00:25:16,120
y nos atacan a nosotros,

286
00:25:16,167 --> 00:25:20,580
como lobos feroces,
como avispones ponzo�osos

287
00:25:20,633 --> 00:25:23,500
�que van de norte a sur
y de este a oeste!

288
00:25:26,967 --> 00:25:30,433
Ahora son una amenaza
a los reinos de Inglaterra.

289
00:25:31,867 --> 00:25:34,580
Antes ven�an y nos saqueaban
para luego irse,

290
00:25:34,633 --> 00:25:36,780
�pero ahora, se quedan!

291
00:25:36,833 --> 00:25:38,620
Azotan constantemente la isla

292
00:25:38,667 --> 00:25:41,820
y reh�san irse
a menos que les paguemos.

293
00:25:41,867 --> 00:25:43,620
York tiene un fuerte vikingo.

294
00:25:43,667 --> 00:25:49,067
�Cu�nto m�s antes de que caigan
m�s pueblos y reinos?

295
00:25:50,467 --> 00:25:54,400
Nuestra tierra y nuestra fe
est�n en gran peligro.

296
00:25:56,100 --> 00:25:58,500
Por eso los he convocado.

297
00:26:00,433 --> 00:26:01,940
Ma�ana,

298
00:26:02,000 --> 00:26:06,420
discutiremos la mejor manera
de enfrentar esta amenaza.

299
00:26:06,467 --> 00:26:08,340
Debemos hacer planes.

300
00:26:08,400 --> 00:26:11,260
Debemos ser fuertes y resolutos.

301
00:26:11,333 --> 00:26:14,740
En esa tarea,
s� que nuestro se�or y salvador,

302
00:26:14,800 --> 00:26:16,650
Jes�s Cristo, nos proteger�.

303
00:26:16,700 --> 00:26:18,660
Si �l est� con nosotros,

304
00:26:19,100 --> 00:26:20,967
no fallaremos.

305
00:27:47,267 --> 00:27:49,000
Te traje a Alfred.

306
00:27:55,667 --> 00:27:59,933
Todos nuestros pecados son
lavados con la sangre de Cristo.

307
00:28:02,033 --> 00:28:03,633
�mense.

308
00:28:04,867 --> 00:28:07,300
Hagan lo que sea
mejor para Wessex...

309
00:28:08,367 --> 00:28:09,800
y nada m�s.

310
00:28:12,200 --> 00:28:13,520
Mi hijo.

311
00:28:38,300 --> 00:28:39,610
Mi hijo.

312
00:28:41,100 --> 00:28:42,340
Padre.

313
00:28:48,000 --> 00:28:50,300
Ven aqu�.

314
00:28:52,467 --> 00:28:54,420
Nunca conoc� a mi madre.

315
00:28:55,100 --> 00:28:57,267
Ella muri� cuando nac�.

316
00:28:58,400 --> 00:29:03,300
Pero ustedes,
deben cuidar a su madre.

317
00:29:04,567 --> 00:29:06,000
Esc�chenla.

318
00:29:08,733 --> 00:29:11,133
Es m�s sabia que ustedes.

319
00:29:21,400 --> 00:29:22,920
Dulce Judith.

320
00:29:26,133 --> 00:29:31,533
No llores por m�.
Los �ngeles ya llegaron.

321
00:29:33,833 --> 00:29:35,300
�No los ves?

322
00:29:36,867 --> 00:29:39,433
�No los ves?

323
00:30:02,067 --> 00:30:03,940
No quieren un acuerdo.

324
00:30:06,433 --> 00:30:08,180
Tenemos que pelear.

325
00:30:08,567 --> 00:30:10,320
Tenemos que pelear.

326
00:30:11,600 --> 00:30:14,560
Al final todos peleamos
por el legado de mi padre.

327
00:30:15,233 --> 00:30:17,020
Todos cre�mos en �l.

328
00:30:17,900 --> 00:30:22,160
Todos sabemos c�mo un joven
granjero de Noruega

329
00:30:22,233 --> 00:30:25,320
arriesg� su vida
para explorar el mundo

330
00:30:25,367 --> 00:30:27,920
para que nuestra gente
pudiera cultivar.

331
00:30:28,333 --> 00:30:30,160
Ese fue su prop�sito.

332
00:30:30,867 --> 00:30:32,660
Esa fue su ambici�n.

333
00:30:34,500 --> 00:30:36,540
Quiero que alcancemos eso.

334
00:30:41,733 --> 00:30:43,240
Si Ivar gana,

335
00:30:46,467 --> 00:30:48,660
el sue�o de Ragnar se perder�.

336
00:30:52,100 --> 00:30:53,600
Prepar�monos.

337
00:30:55,033 --> 00:30:57,740
Hay que cosas que discutir
primero, �cierto?

338
00:30:59,433 --> 00:31:02,920
No podemos dejar Kattegat
totalmente expuesta.

339
00:31:02,967 --> 00:31:05,840
No conocemos los planes de Ivar.

340
00:31:05,900 --> 00:31:09,320
Pienso que deber�amos enviar
un grupo de guerreros

341
00:31:09,400 --> 00:31:11,520
a defender el pueblo.

342
00:31:11,600 --> 00:31:15,160
No sabes lo que vi,
el n�mero de francos.

343
00:31:15,233 --> 00:31:20,200
Los dioses ya han decidido
el resultado de esta batalla.

344
00:31:28,500 --> 00:31:30,667
El concejo se reunir� ma�ana.

345
00:31:38,600 --> 00:31:41,467
Deben elegir un nuevo rey.

346
00:31:46,467 --> 00:31:48,380
Te ofrecer�n la corona.

347
00:31:49,200 --> 00:31:50,760
�Est�s segura?

348
00:31:54,667 --> 00:31:56,040
S�.

349
00:31:56,100 --> 00:31:59,800
Adem�s de ser el hijo mayor
de Aethelwulf,

350
00:31:59,867 --> 00:32:03,933
saben que te estuvo entrenando
para ser rey.

351
00:32:07,567 --> 00:32:12,740
Adem�s, no hay nadie,
ni siquiera Cuthred,

352
00:32:12,800 --> 00:32:16,140
que tenga el poder
para reclamar la corona.

353
00:32:16,200 --> 00:32:20,433
Entonces aceptar� con humildad.

354
00:32:22,433 --> 00:32:23,540
No.

355
00:32:25,667 --> 00:32:27,567
Rechazar�s la corona.

356
00:32:30,200 --> 00:32:31,580
�Por qu� har�a eso?

357
00:32:31,633 --> 00:32:34,320
Dijiste que mi padre
me prepar� para eso.

358
00:32:34,367 --> 00:32:37,320
�S�, Aelthelred!

359
00:32:37,400 --> 00:32:41,920
Te prepar� para ser rey
a su imagen y semejanza,

360
00:32:41,967 --> 00:32:44,440
un rey guerrero.

361
00:32:44,500 --> 00:32:47,760
Pero estos tiempos exigen
otro tipo de gobernante,

362
00:32:47,833 --> 00:32:49,940
tu abuelo lo dej� muy claro.

363
00:32:50,000 --> 00:32:52,340
�Qu� podr�a saber
Ecbert de estos tiempos?

364
00:32:52,400 --> 00:32:53,360
Todo.

365
00:32:53,401 --> 00:32:56,960
�No! �Solo lo mencionas
porque fuiste su amante!

366
00:32:57,033 --> 00:32:59,920
Lo cual va en contra
de las costumbre cristianas.

367
00:32:59,967 --> 00:33:01,840
- Hermano.
- �No hables!

368
00:33:01,900 --> 00:33:04,840
�Al contrario! �Debe hacerlo!

369
00:33:04,900 --> 00:33:07,380
No importa las circunstancias
de su nacimiento,

370
00:33:07,433 --> 00:33:11,480
�Ecbert vio en Alfred
al futuro gobernante que cre�!

371
00:33:11,533 --> 00:33:13,080
�S�!

372
00:33:13,133 --> 00:33:17,800
Por eso envi� a Alfred
a Roma y no a ti, Aelthelred.

373
00:33:17,867 --> 00:33:20,140
Lo envi� a �l.

374
00:33:20,200 --> 00:33:24,960
El Papa lo bendijo y lo coron�

375
00:33:25,033 --> 00:33:27,240
porque lo sab�a.

376
00:33:27,300 --> 00:33:29,360
�No!

377
00:33:29,433 --> 00:33:30,480
�No lo sab�a!

378
00:33:30,533 --> 00:33:32,280
Pudo haber sido yo.

379
00:33:36,300 --> 00:33:38,000
Pero no fuiste t�.

380
00:33:43,600 --> 00:33:46,320
Ecbert vio en Alfred

381
00:33:46,367 --> 00:33:49,867
dones que t� no ten�as.

382
00:33:51,633 --> 00:33:56,040
Habilidades que t� no ten�as.

383
00:33:56,100 --> 00:34:00,160
Lo educ� en el arte de gobernar.

384
00:34:00,233 --> 00:34:05,100
Lo equip� para ser
el futuro rey.

385
00:34:22,767 --> 00:34:24,240
Pero me pedir�n que sea rey.

386
00:34:24,300 --> 00:34:25,640
S�.

387
00:34:25,700 --> 00:34:27,867
Estoy segura de que lo har�n.

388
00:34:28,900 --> 00:34:30,240
Muy bien.

389
00:34:39,433 --> 00:34:43,033
Ma�ana el consejo
elegir� al nuevo rey.

390
00:34:46,867 --> 00:34:50,667
Debe elegir al pr�ncipe Alfred.

391
00:34:52,067 --> 00:34:53,180
Mi Se�ora, no es sabio...

392
00:34:53,233 --> 00:34:57,400
Elegir� a Alfred

393
00:34:57,467 --> 00:34:59,280
y usted, Lord Cuthred,

394
00:34:59,333 --> 00:35:02,360
ser� el primero en apoyarlo.

395
00:35:02,433 --> 00:35:05,800
Mi conciencia
no me permite hacerlo.

396
00:35:05,867 --> 00:35:10,360
Digamos que est�
dispuesto a hacerlo.

397
00:35:10,433 --> 00:35:15,360
�Su conciencia le permitir�,
como recompensa,

398
00:35:15,433 --> 00:35:18,867
aceptar la posici�n
de Obispado de Sherborne?

399
00:35:20,700 --> 00:35:22,320
Como sabe,

400
00:35:22,400 --> 00:35:26,880
una posici�n tan alta confiere
mayor poder y prestigio

401
00:35:26,933 --> 00:35:30,400
que el que gozan
los lores temporales.

402
00:35:31,367 --> 00:35:33,360
Y por supuesto, Mi Se�or,

403
00:35:33,400 --> 00:35:35,800
usted ha aspirado
por mucho tiempo

404
00:35:35,867 --> 00:35:40,700
a ser Pr�ncipe
de la Santa Iglesia.

405
00:35:59,233 --> 00:36:00,740
Como sabemos,

406
00:36:02,700 --> 00:36:04,420
el Rey Aethelwulf,

407
00:36:05,700 --> 00:36:07,620
Rey de Wessex y Mercia,

408
00:36:08,533 --> 00:36:13,880
Bretwalda y mi amado padre,
est� muerto.

409
00:36:13,933 --> 00:36:16,480
Ahora descansa
en la gloria del Creador.

410
00:36:21,300 --> 00:36:23,780
A�n no conocemos
la causa de su muerte.

411
00:36:23,833 --> 00:36:26,760
Fue tan repentino,
una calamidad.

412
00:36:26,833 --> 00:36:29,480
C�mo nos fue arrebatado
un gran guerrero

413
00:36:29,533 --> 00:36:31,080
en esta hora de necesidad,

414
00:36:31,133 --> 00:36:33,440
est� fuera
de nuestra comprensi�n.

415
00:36:34,133 --> 00:36:36,760
Nuestro se�or obra
de formas misteriosas.

416
00:36:36,833 --> 00:36:40,400
Pero ahora debemos
elegir a un nuevo rey.

417
00:36:41,800 --> 00:36:44,600
Ese es nuestro deber
solemne y sagrado.

418
00:36:46,200 --> 00:36:51,120
No me cabe la menor duda
al nominar a su hijo mayor,

419
00:36:51,167 --> 00:36:55,740
el Pr�ncipe Aethelred.

420
00:36:55,800 --> 00:36:58,920
Que sea �l elegido
el nuevo gobernante

421
00:36:59,000 --> 00:37:02,760
para enfrentar los nuevos
desaf�os que nos esperan.

422
00:37:02,833 --> 00:37:07,733
�Digo que elijamos a Aethelred
como nuestro rey!

423
00:37:19,667 --> 00:37:22,380
Gracias, mi Se�or Cuthred
por mi nominaci�n.

424
00:37:24,767 --> 00:37:27,600
Ser su rey ser�
el m�s grande honor de mi vida,

425
00:37:29,300 --> 00:37:31,680
incluso en estos tiempos
de peligro.

426
00:37:40,033 --> 00:37:41,780
Desafortunadamente,

427
00:37:43,367 --> 00:37:44,920
debo declinar.

428
00:37:46,600 --> 00:37:49,000
No estoy calificado
para ser su rey.

429
00:37:50,233 --> 00:37:52,300
Deben elegir a alguien m�s.

430
00:37:55,333 --> 00:37:56,720
�Silencio!

431
00:37:57,533 --> 00:37:59,740
�Silencio, por favor!

432
00:37:59,800 --> 00:38:02,320
Tenemos que tomar
una decisi�n grande.

433
00:38:02,367 --> 00:38:03,760
Por favor.

434
00:38:04,733 --> 00:38:08,540
En vista de que Aethelred,
el hijo mayor,

435
00:38:08,600 --> 00:38:10,320
rechaz� la corona,

436
00:38:11,500 --> 00:38:16,740
entonces creo que debemos
ofrec�rsela a su hijo menor,

437
00:38:16,800 --> 00:38:18,533
Pr�ncipe Alfred.

438
00:38:19,800 --> 00:38:21,400
�Todos a favor?

439
00:38:22,667 --> 00:38:24,520
�No es un guerrero!

440
00:38:24,567 --> 00:38:25,480
<i>�Es muy d�bil!</i>

441
00:38:25,534 --> 00:38:28,080
<i>�No est� calificado
para ser rey!</i>

442
00:38:31,600 --> 00:38:33,240
Mis se�ores.

443
00:38:33,300 --> 00:38:34,320
Mis se�ores.

444
00:38:34,367 --> 00:38:37,100
�Silencio, por favor!

445
00:38:38,633 --> 00:38:41,100
Por el bien de Wessex
y de Inglaterra,

446
00:38:42,200 --> 00:38:44,760
en nombre de mi padre,
el Rey Aethelwulf

447
00:38:45,767 --> 00:38:47,960
y de mi abuelo, el Rey Ecbert,

448
00:38:51,833 --> 00:38:57,140
nomino a mi hermano,
Alfred, para ser rey.

449
00:38:57,200 --> 00:38:59,900
Todos los que est�n
de acuerdo digan "aye".

450
00:39:00,833 --> 00:39:01,840
<i>�Aye!</i>

451
00:39:01,900 --> 00:39:03,680
<i>�Aye!</i>

452
00:39:03,733 --> 00:39:04,820
�Aye!

453
00:39:04,867 --> 00:39:09,640
<i>�Aye!</i>

454
00:39:09,700 --> 00:39:10,900
�Aye!

455
00:39:46,200 --> 00:39:48,360
�Hay un incendio!
�Vengan, r�pido!

456
00:39:48,400 --> 00:39:51,240
�Hay un incendio en el templo!
�Vengan, r�pido!

457
00:39:52,733 --> 00:39:55,880
<i>�El templo est� en llamas!
�Thor est� en llamas!</i>

458
00:41:49,467 --> 00:41:53,060
He venido a ofrecerte
mi lealtad, Reina Lagertha.

459
00:41:53,133 --> 00:41:56,420
Mi espada est� a tu servicio,

460
00:41:56,467 --> 00:41:58,867
ahora y por siempre.

461
00:42:18,300 --> 00:42:19,780
�Bul!

462
00:42:19,833 --> 00:42:21,940
Bul el perro, t� lo hiciste.

463
00:42:26,233 --> 00:42:28,460
<i>�Larga vida al Rey!</i>

464
00:42:28,533 --> 00:42:30,700
<i>�Larga vida al Rey!</i>

465
00:42:30,767 --> 00:42:32,980
<i>�Larga vida al Rey!</i>

466
00:42:33,033 --> 00:42:35,340
<i>�Larga vida al Rey!</i>

467
00:42:35,400 --> 00:42:37,540
<i>�Larga vida al Rey!</i>

468
00:42:37,600 --> 00:42:40,020
<i>�Larga vida al Rey!</i>

469
00:42:40,067 --> 00:42:42,200
<i>�Larga vida al Rey!</i>

470
00:42:56,533 --> 00:42:59,540
Quemar a los dioses,
fuiste demasiado lejos, Bul.

471
00:42:59,600 --> 00:43:01,260
No lo creo.

472
00:43:01,333 --> 00:43:02,833
�Bul, no!

473
00:43:16,800 --> 00:43:18,720
<i>�Larga vida al Rey!</i>

474
00:43:18,767 --> 00:43:21,200
<i>�Larga vida al Rey!</i>

475
00:43:25,100 --> 00:43:26,620
�No!

476
00:43:26,667 --> 00:43:29,540
<i>�No!</i>

477
00:43:29,600 --> 00:43:31,767
<i>�No!</i>

478
00:43:33,033 --> 00:43:34,967
<i>�No!</i>

