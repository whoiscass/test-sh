import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { FileUploadModule } from 'ng2-file-upload';

import { app_routing } from './app.router';
import { RequestService } from './services/request.service';


import { AppComponent } from './app.component';
import { WrapperComponent } from './components/wrapper/wrapper.component';
import { SingleExerciseComponent } from './components/single-exercise/single-exercise.component';


@NgModule({
  declarations: [
    AppComponent,
    WrapperComponent,
    SingleExerciseComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    FileUploadModule,
    app_routing
  ],
  providers: [RequestService],
  bootstrap: [AppComponent]
})
export class AppModule { }
