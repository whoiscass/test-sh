import { RouterModule, Routes } from '@angular/router';
import { WrapperComponent } from './components/wrapper/wrapper.component';
import { SingleExerciseComponent } from './components/single-exercise/single-exercise.component';

const app_routes: Routes = [
  { path: '', component: WrapperComponent },
  { path: 'exercises/:id', component: SingleExerciseComponent },
  { path: '**', pathMatch: 'full', redirectTo: '' },
];

export const app_routing = RouterModule.forRoot(app_routes);