import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RequestService } from '../../services/request.service';
import { FileSelectDirective, FileUploader } from 'ng2-file-upload';

@Component({
  selector: 'app-single-exercise',
  templateUrl: './single-exercise.component.html',
  styleUrls: ['./single-exercise.component.css']
})
export class SingleExerciseComponent implements OnInit {
  id: any;
  exercise: any;
  levels: Array<Number> = [0, 1, 2, 3, 4, 5];
  status: Array<String> = ['enable', 'disable'];
  uploader: FileUploader = new FileUploader({url: 'http://localhost:5000/exercises/upload'});
  uploaderv: FileUploader = new FileUploader({url: 'http://localhost:5000/exercises/upload'});
  uploaderi: FileUploader = new FileUploader({url: 'http://localhost:5000/exercises/upload'});

  constructor(
    private route: ActivatedRoute,
    private request: RequestService
  ) {
    this.uploader.onAfterAddingFile = (file: any) => {
      file.withCredentials = false;
    };
    this.uploaderv.onAfterAddingFile = (file: any) => {
      file.withCredentials = false;
    };
    this.uploaderi.onAfterAddingFile = (file: any) => {
      file.withCredentials = false;
      if(this.uploaderi.queue.length > 1) {
        this.uploaderi.queue.shift();
      }
      console.log(this.uploaderi.queue);
    };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      let asd = [].push(JSON.parse(response));
      this.exercise.music = asd;
    }
    this.uploaderv.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      let qwe = [].push(JSON.parse(response));
      this.exercise.videoTutor = qwe;
    }
    this.uploaderi.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.exercise.mainImage = JSON.parse(response).sourcePath;
    }
  }

  ngOnInit() {
    this.route.params.subscribe(p => {
      this.id = p.id;
    });
    this.request.getExercise(this.id)
    .subscribe(r => {
      r.map(i => {
        this.exercise = i;
        console.log(i);
      });
    });
  }

  onSubmit() {
    if(this.uploader.queue.length) this.uploader.uploadAll();
    if(this.uploaderv.queue.length) this.uploaderv.uploadAll();
    if(this.uploaderi.queue.length) this.uploaderi.uploadAll();
    setTimeout(() => { 
      this.request.updateExercise(this.exercise._id, this.exercise)
      .subscribe(r => {
        console.log(r);
      })
    }, 2000);
  }

  download(name) {
    this.request.getFile(name)
    .subscribe(r => {
      console.log(r);
    });
  }

}
