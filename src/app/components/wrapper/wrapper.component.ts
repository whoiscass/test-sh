import { Component, OnInit } from '@angular/core';
import { FileSelectDirective, FileUploader } from 'ng2-file-upload';
import { RequestService } from '../../services/request.service';

@Component({
  selector: 'app-wrapper',
  templateUrl: './wrapper.component.html',
  styleUrls: ['./wrapper.component.css']
})
export class WrapperComponent implements OnInit {
  exercises: Array<any> = [];
  levels: Array<Number> = [0, 1, 2, 3, 4, 5];
  status: Array<String> = ['enable', 'disable'];
  exercise: any = {
    name: '',
    typeExercise: '',
    mainImage: '',
    duration: 0,
    lights: [],
    music: [],
    video: [],
    createdBy: ''
  };
  uploader: FileUploader = new FileUploader({url: 'http://localhost:5000/exercises/upload'});
  uploaderv: FileUploader = new FileUploader({url: 'http://localhost:5000/exercises/upload'});
  uploaderi: FileUploader = new FileUploader({url: 'http://localhost:5000/exercises/upload'});

  constructor(private request: RequestService) {
    this.uploader.onAfterAddingFile = (file: any) => {
      file.withCredentials = false;
    };
    this.uploaderv.onAfterAddingFile = (file: any) => {
      file.withCredentials = false;
    };
    this.uploaderi.onAfterAddingFile = (file: any) => {
      file.withCredentials = false;
      if(this.uploaderi.queue.length > 1) {
        this.uploaderi.queue.shift();
      }
      console.log(this.uploaderi.queue);
    };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.exercise.music.push(JSON.parse(response));
      this.exercise.music.filter(m => {
        m.volumen = this.exercise.music.indexOf(m) + 1;
      });
    }
    this.uploaderv.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.exercise.video.push(JSON.parse(response));
    }
    this.uploaderi.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.exercise.mainImage = JSON.parse(response).sourcePath;
    }
  }

  ngOnInit() { 
    this.request.getExercises()
    .subscribe(r => {
      r.map(i => {
        this.exercises.push(i);
      });
    });
    console.log(this.exercises);
  }

  onSubmit() {
    if(this.uploader.queue.length) this.uploader.uploadAll();
    if(this.uploaderv.queue.length) this.uploaderv.uploadAll();
    if(this.uploaderi.queue.length) this.uploaderi.uploadAll();
    setTimeout(() => { 
      this.request.createExercise(this.exercise)
      .subscribe(r => {
        this.exercises.push(r);
      });   
    }, 2000);
  }

  remove(id: any) {
    this.request.deleteExercise(id)
    .subscribe(r => {
      this.exercises.splice(this.exercises.indexOf(r), 1);
    });
  }

}
