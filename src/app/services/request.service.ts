import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';

@Injectable()
export class RequestService {
  exercises: Array<String> = [];

  constructor(private http: HttpClient) { }

  createExercise(exercise) {
    return this.http.post('http://127.0.0.1:5000/exercises', exercise)
    .map((response: any) => {
      return response;
    });
  }

  getExercises() {
    return this.http.get('http://127.0.0.1:5000/exercises')
    .map((response: any) => {
      return response;
    });
  }

  getExercise(id) {
    return this.http.get('http://127.0.0.1:5000/exercises/' + id)
    .map((response: any) => {
      return response;
    });
  }

  deleteExercise(id) {
    return this.http.delete('http://127.0.0.1:5000/exercises/' + id)
    .map((response: any) => {
      return response;
    });
  }

  updateExercise(id, modified) {
    return this.http.put  ('http://127.0.0.1:5000/exercises/' + id, modified)
    .map((response: any) => {
      return response;
    });
  }

  getFile(name) {
    return this.http.post('http://127.0.0.1:5000/exercises/download', name, {
      responseType: 'blob',
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    })
    .map((response: any) => {
      return response;
    });
  }

}
